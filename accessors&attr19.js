//Аксессоры и аттрибуты свойств

var person = {
    name: "Dima",
    _age: 30,
    get age(){
        return this._age;
    },
    set age(value){
        this._age = value < 0 ? 0 : value > 122 ? 122 : value;
    }
};


person.age = 144;
console.log(person.age);
person.age = -10;
console.log(person.age);
person.age = 33 ;
console.log(person.age);
// person.age;
//для получения дескриптора объекта
console.log(Object.getOwnPropertyDescriptor(person, "name"));
console.log(Object.getOwnPropertyDescriptor(person, "age"));

//для определения или переопределения атрибутов свойств 
Object.defineProperty(person, "gender", {
    value: "male",
    writable: false,
    enumerable: true,//указывает это свойство перечисляемое или нет
    configurable: false,
});

console.log(person.gender);

person.gender = "female";

console.log(person.gender);
console.log("");

// цикл for in
//для перебора всех перечисляемых свойст объекта person
for (property in person){
    console.log(property);
}
//или можно использовать метод keys объекта Object
console.log(Object.keys(person));


//проверка атрибута enumerable
console.log(person.propertyIsEnumerable("gender"));

var o = {}; 
Object.defineProperties(o, {
    x:{
        value: 20,
        writable: false,
    },
    y:{
        value: 10,
        writable: false,
    },
    r:{
        get: function () {
            return Math.sqrt(this.x * this.x + this.y * this.y );
        }
    }
});

console.log(o.r);

//расширяемость объектов, возможность добавления новых свойств 


var obj = {};
console.log(Object.isExtensible(obj));//true
Object.preventExtensions(obj);//запрещает расширение объекта
console.log(Object.isExtensible(obj));//false 

obj.x = 234234;

console.log(obj.x);