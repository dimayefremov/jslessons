//циклы

// loop for
//for(initial; test; increment){ loop body }
// for (;;); - бесконечный цикл
var z;
for (z = 0; z < 10; z++) {
    console.log(z);
}


//цикл  до 0(декримент) работает быстрее
for (i = 10; i > 0; i--) {
    console.log(i);
}
var b;
//аналогично
for (b = 10; b--;) {
    console.log(b);
}

/* цикл while
while (condition) {
    expression
}*/
var a = 10;
while (a--) {
    console.log(a);
}

//do while

// do {
//     expression
// } while (condition);
