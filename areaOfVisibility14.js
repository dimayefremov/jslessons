//scope, scope chane

//приоритет

var i = 5;
var func = function() {
    var i = 10;
    console.log(i);
    var innerFunc = function () {
        console.log(i);//undefined
        var i = 15;
        console.log(i);
    }
    innerFunc();//15
    console.log(i);//10//если не поставить var то 15 
}


func();// 10
//при объявлении локальной переменной надо писат ключевое слово var, или мы будем изменять эту же переменную находящуюся в следущей области видимости по цепочке

