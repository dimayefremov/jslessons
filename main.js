/*целочисленные литералы*/
var num;
num = 123 + 123;
console.log(0xfffcc);//шестнацатеричное число
console.log(0345);//восьмеричное число
console.log(num);//десятичное
/* конец*/
/*вещественные*/
console.log(12.45);//c плавающей точкой
console.log(0.45);//c плавающей точкой
console.log(.45);//c плавающей точкой
console.log(12.34e4);//экспоненциальная форма
/*конец*/


// преобразование с одной формы в другую
//объекты обертки
var  N = new Number(4000);

console.log(typeof N);
console.log(N.toFixed(2));

var n = 5000;
console.log(n.toFixed(2));
console.log(2 .toFixed(2));//числовой литерал
console.log(n.toExponential(4));
console.log(n.toPrecision(3));

//арифметические операторы
// унарный +
console.log(+10);

// унарный -
console.log(-10);


//инкремент ++
var i = 10;
console.log(++i);
//декремент --
console.log(i--);
console.log(i);

//бинарные операторы
console.log(5 + 10);
console.log(7 - 5);
console.log(5 * 7);
console.log(8 / 2);
console.log(8 % 3);


//присваивание с операцией
var n = 100;
n = n + 20;
console.log(n);
var i = 100;
i -= 20;
console.log(i);

//операторы отношения
console.log(5 > 10);
console.log(5 < 10);
console.log(5 >= 10);
console.log(5 <= 10);
console.log(10 == 10);
console.log(10 === "10");
console.log(5 !== 10);

//объект Math
console.log(Math.sqrt(7));//извлечение квадратного корня
console.log(Math.pow(7,3));//возведение в степень
console.log(Math.PI);//constana
console.log(Math.E);//constana


// string
console.log("some text");
console.log("some \"text\"");
console.log('test');
console.log('test "long"\
    egefgr');
console.log('test "long"'.length);
// оператор + являеться конкатенацией если один из операндов СТРОКА
var string = "Hello"
console.log(string + " world");//конкатенация


var words = "some text same is different";
//строка начинается с 0-го индекса
// строка не изменяемый тип в js те все следующие методы не изменяют строку
console.log(words.length);//длинна строки
console.log(words.charAt(3));//вернет символ стоки под указанным индексом
console.log(words.substring(10));//получим строку с 10-го символа и до конца
console.log(words.replace('same', 'not same'));//замена строки поиска(только при выводе)

console.log(words.split(" "))//принимает разделител(сейчас ПРОБЕЛ) и разбивает строку на массив по этому разделителю

console.log(words.toUpperCase());//приводит к верхнему регистру
console.log(words.toLowerCase());//приводит к нижнему регистру

console.log(words[5]);//строки могут интерпретироваться как объекты



console.log(true);//true
console.log(false);//false
console.log(5===5);//true
console.log(5===8);//false
console.log(Boolean(5));//true


console.log(Boolean(undefined));//false
console.log(Boolean(null));//false
console.log(Boolean(0));//false
console.log(Boolean(NaN));//false
console.log(Boolean(""));//false
