console.log(typeof String(Infinity));//string
console.log(typeof String(NaN));//string
console.log(+"");// 0


//короткая запись приведения к булевым значениям
console.log(!!"");//false
console.log(!!NaN);//false
console.log(!!0);//false
console.log(!!null);//false
console.log(!!undefined);//false


console.log(!!"Hi");//true
console.log(+"          3 f");//NaN
console.log(parseInt("4 px"));//4


//преобразование булевых значений в числа
console.log(parseInt(+true));//1
console.log(parseInt(+false));//0
