//замыкания



//лексическая область видимости
/*var func = function(){
    var i = 10;
    return  function () {
        return  i;
    }
};

var myFunc = func();

var anotherFunc = function () {
    var i = 20;
    console.log(myFunc());
};

anotherFunc();//10*/

//счетчик
/*var counter = (function () { //анонимная самовызывающаяся функция
    var count = 0;
    return function (num) {
        count = num !== undefined ? num : count;
        return count++;
    }
}());

console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter(30));
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());*/

// тот же счетчик только через объект

var counter = function (num) {
    counter.count = num !== undefined ? num : counter.count;
    return counter.count++;
}

counter.count = 0;
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter(30));
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());