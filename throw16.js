//обработка ошибок, исключений


// 10 = 'string'

// var myError = new Error('My Error Message');
// console.log(myError.name);
// console.log(myError.message);


// throw myError;

// try {
//  //инструкция для выполнения которая может бросить исключение
// } catch (identity /*(выражение которое было в инструкции throw)*/) {
//     //ловим исключение, помещаем код который будет обрабатывать исключения
// } finally {
//     //инструкции которые будут выполняться вне зависимости произошли ли какието исключения в блоке try или нет
// }


var calculate = function (n) {
    if (n > 10) {
        throw new Error("n should be less than 10");
    }
    return n + 10;
}


try{
    calculate(1)
}catch(e){
    console.log("Can`t execute calculate: " + e.message);
}