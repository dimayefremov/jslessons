//#23 - JSON

var user = {
    name: "Frank",
    id: 42234,
    lastVisit: Date.now(),
    friends: [234, 23523, 2344],
    //если присутствует метод toJson преобразовываться в строку будет только то что возвращает этот метод
    toJSON: function() {
        return {
            name: this.name,
            lastVisit: this.lastVisit
        }
    }    
};

var userData = JSON.stringify(user);

console.log(userData);
console.log(JSON.parse(userData));