//objects in js

/*{
    имя: значение, 
    имя: значение, 
    имя: значение 
}*/

//1) объектный литерал 
var person = {
    name: 'Dima',
    age: 20,
    gender: "male",
    sayHi: function () {
        return "Hello!!!";
    }
}


person.age = 33;// изменили свойство age в объекте
person.userId = 234234; // добавили новое св-во в объект

// console.log(person.name);
// console.log(person.age);

// console.log(person["gender"]);

// console.log(person);

// console.log(person.sayHi());

//2) создание объекта с помошью ключевого слова new и функции-конструктора
// var object = new Object();
// object.property = "value"

//3)способ создания объекта с помошью статического метода create класса Object

var object = Object.create({x: 10, y: 20});
object.x = 20;

console.log(object);


console.log(object);
console.log(object.x);

// delete выражение

delete object.x
// console.log(object);

// console.log("x" in object);
// console.log("o" in object);

/*console.log(object.x);
console.log(object.o);
*/

console.log(object.z);//undefined
console.log("z" in object);//false

object.z = undefined;

console.log(object.z);//undefined
console.log("z" in object);//true