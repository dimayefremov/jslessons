//функции в javascript являються ОБЪЕКТАМИ(OBJECTS)

/*function идентификатор(параметры/аргументы{
    инструкции
    return выражения
}*/
//инструкция объявления функции
/*function greet(name) {
    return "Hello " + name;
}*/

//выражение определения функции, можно опускать имя функции, такая функция называеться - АНОНИМНОЙ
/*var greet = function(name) {
    console.log(arguments.length);// для получения всех переданных аргументов, это объект ведет себя как массив
    return "Hello " + name;
};

console.log(greet('Dima', 34,22,33, 33).toUpperCase());*/

//передача функции в качесве аргумента

/*var func = function(callback){
    var name = 'Dimas';

    callback(name);
};

func(function(n){
console.log("Hello "+ n);
});*/

//функция может быть возвращаемым значением, в примере возвращаеться анонимная функция
/*var anonim = function() {
    return function () {
        console.log('test');
    }
};

anonim()();*/

// функция может быть вызвана сразу после определения, если сразу использовать выражение определения
//переменной присваиваетсья значение функции ,для большей ясности брать выражение в круглые скобки
var greeting = (function(name) {
    return "Hello " + name;
}('TEst'));

console.log(greeting);