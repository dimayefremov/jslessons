// #25 - ES5 методы массивов
//все здесь рассмотренные методы не изменяют исходный массив, а возвращают ноывй массив

// var t = window.prompt('eeee')
// console.log(t);

var array = ["some string", "another string", "Third string", "javascript", "dimas"];

//foreach method содержит коллбэк функцию которая будет применяться для каждого элемента массива
array.forEach(function(element, index, array) {});//element - ccылка на элемент массива
// index - индекс этого элемента: array - сам массив

// array.forEach(function(element, index, array){
// 	array[index] = element.toUpperCase();
// });

console.log(array);

// Метод map() создаёт новый массив с результатом вызова указанной функции для каждого элемента массива.
console.log(array.map(function(e) {return e.toUpperCase()}));


// Метод filter() создаёт новый массив со всеми элементами, прошедшими проверку, задаваемую в передаваемой функции.

var filtered = array.filter(function(e) { return e.indexOf('o') === -1 });

console.log(filtered);

//Метод every() проверяет, удовлетворяют ли все элементы массива условию, заданному в передаваемой функции.
console.log(array.every(function(e) {return e.length >= 5}));



// Метод reduce() применяет функцию к аккумулятору и каждому значению массива (слева-направо), сводя его к одному значению.
var numbers = [1, 2, 3, 3, 3, 4, 5, 6];


var reduced = numbers.reduce(function (a, b) {
	return a * b;
});

console.log(reduced);


//следующие методы не принимают коллбэк функции
console.log(numbers.indexOf(3));

console.log(numbers.lastIndexOf(3));


