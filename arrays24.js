var months = ["january", 'february', 'March', "April"];

months[4] = 'may';

console.log(months);
console.log(months.length);

months[months.length] = "june";
console.log(months);

months[20] = "Somethinng";
console.log(months);
console.log(months.length);
console.log(typeof months);

//функция конструктор у массива
var myArray = new Array(11,1131, 212);//элементы которые передаються в конструктор будут элементами массива, кроме того случая когда передается один аргумент
console.log(myArray);
months["someProperty"] = "someValue";//можно использовать строки вместо индексов
console.log(months); // индексы массивов не могут быть отрицательными


console.log(months.join('||'));//восвращает элементы массива сконкатенировав их в одну строку
console.log(months.sort());//сортирует массив в алфавитном порядке

console.log(months.concat("hello", ['Event more', "strings"]));//складывает массив с др массивом или с другими элементами, и возвращает новый массив
console.log(months.slice(21));//возвращает новый массив, содержащий копию части исходного массива.


console.log(months.push('push'));//добавляет элемент в конец массива
console.log(months.unshift('unshift'));//добавляет элемент в начало массива

console.log(months);

console.log(months.pop());//удаляет последний элемент массива и возвращает его
console.log(months.shift());//удаляет первый элемент массива и возвращает его


