// this и непрямой вызов методов 
//родные методы функций bind, call, apply


var greet = function (greeting) {
    return  greeting + "! my name is " + this.name;
}

var person = {
    name: "John",
    greet: greet
};
var anotherPerson = {
    name: "Bob",
    greet: greet
};

console.log(person.greet());//this указывает на объект person
console.log(anotherPerson.greet());//this указывает на объект anotherPerson
console.log(greet());//указывает на глобальный объект (window)
console.log(this);//указывает на глобальный объект (window)


console.log(anotherPerson.greet.call(person, "Bonjour"));//this будет обращаться к объекту person и выведет name: Bob
                                                        //если функция greet принимает параметры то "Bonjour" будет передаваться как аргумент 

console.log(anotherPerson.greet.apply(anotherPerson, ['Privet']));//тоже самое что и call только аргументы передаються массивом


var bound = greet.bind(anotherPerson);//bind похож на call и apply, но в отличие от них не вызывает функцию, а просто связывает ее с объектом
                                    //и когда мы вызовем наш объект ключевое слово this будет указывать на связаный объект
console.log(bound('Hello there'));//метод bind не изменяет исходную функцию а возвращает новую